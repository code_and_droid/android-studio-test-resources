The test classpaths are set up incorrectly by Android Studio 1.0.1.

When run via CLI gradle (`./gradlew build`), the tests pass, and the relevant bits of classpath look like this:

```
/path/to/proj/android-studio-test-resources/pure-java-subproject/build/classes/test
/path/to/proj/android-studio-test-resources/pure-java-subproject/build/resources/test
/path/to/proj/android-studio-test-resources/pure-java-subproject/build/classes/main
/path/to/proj/android-studio-test-resources/pure-java-subproject/build/resources/main
```

When run via AS, the classpath looks like:

```
/path/to/proj/android-studio-test-resources/pure-java-subproject/build/classes/test
```

Unsurprisingly, without the `resources` directories on the classpath, the tests fail.
